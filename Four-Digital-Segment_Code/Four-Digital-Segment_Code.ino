// https://github.com/MiguelPynto/ShiftDisplay
#include <ShiftDisplay2.h>


const int latch=9;  //74HC595  pin 9 STCP
const int clock=10; //74HC595  pin 10 SHCP
const int data=8;   //74HC595  pin 8 DS


ShiftDisplay2 display(latch, clock, data, COMMON_CATHODE, 4, STATIC_DRIVE);

const int ledPin=5;
const int buttonPin=6;

//Refer Table 7-Segment Decoding
const unsigned char table[]=
{0x3f,0x06,0x5b,0x4f,0x66,0x6d,0x7d,0x07,0x7f,0x6f,0x77,0x7c
,0x39,0x5e,0x79,0x71,0x00};

int ledState = LOW;
int count = 0;
void setup() {
  pinMode(ledPin,OUTPUT);
  pinMode(buttonPin, INPUT_PULLUP);
  
  pinMode(latch,OUTPUT);
  pinMode(clock,OUTPUT);
  pinMode(data,OUTPUT);

  //enabling serial communication
  Serial.begin(57600);
  
  // set initial LED state
  digitalWrite(ledPin, ledState);

  // if analog input pin 0 is unconnected, random analog
  // noise will cause the call to randomSeed() to generate
  // different seed numbers each time the sketch runs.
  // randomSeed() will then shuffle the random function.
  randomSeed(analogRead(0));
}



void loop() {
  count++;
  Serial.print(count);
  int buttonState = digitalRead(buttonPin);
  Serial.print("Button status: ");
  Serial.println(buttonState);
  
  digitalWrite(ledPin, !buttonState);
  int waitMs = 500;
  display.set(8888);
  display.update();
  delay(waitMs);//delay 2 sencond
 
 
}
