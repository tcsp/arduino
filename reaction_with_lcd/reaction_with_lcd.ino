// include the library code:
#include <LiquidCrystal.h>

// LCD Display
const int lcdRSpin = 7;
const int lcdEpin = 6;
// Using only 4 data lines
const int lcdD4pin = 5;
const int lcdD5pin = 4;
const int lcdD6pin = 3;
const int lcdD7pin = 2;

// LED and button
const int ledPin = 8;
const int buttonPin = 9;

const unsigned long minRandomNumber = 2000;     // minimum number used to specify the range of random number
const unsigned long maxRandomNumber = 5000;     // maximum number used to specify the range of random number

uint32_t timeStart, timeEnd;

enum State_enum {INITIAL, WAIT_BUTTON, WAIT_LIGHT, LIGHT_ON, SHOW_RESULT};

uint8_t state = INITIAL;

// initialize the library with the numbers of the interface pins
LiquidCrystal lcd(lcdRSpin, lcdEpin, lcdD4pin, lcdD5pin, lcdD6pin, lcdD7pin);

void setup() {
  pinMode(ledPin, OUTPUT);
  pinMode(buttonPin, INPUT_PULLUP);

  //enabling serial communication
  Serial.begin(57600);

  // if analog input pin 0 is unconnected, random analog
  // noise will cause the call to randomSeed() to generate
  // different seed numbers each time the sketch runs.
  // randomSeed() will then shuffle the random function.
  randomSeed(analogRead(0));

  // set up the LCD's number of columns and rows:
  lcd.begin(16, 2);
}

void loop() {
  int buttonState = digitalRead(buttonPin);

  switch (state)
  {
    case INITIAL:
      Serial.println("Cy is learning to program");
      // Print a message to the LCD.
      lcd.clear();
      lcd.print("Hola :-)  Pica!");
      state = WAIT_BUTTON;
      break;

    case WAIT_BUTTON:
      delay(150); // leave time to lift up button
      if (buttonState == LOW) {
        // button pressed
        lcd.clear();
        lcd.print("Atencion!");
        state = WAIT_LIGHT;
      }
      break;

    case WAIT_LIGHT:
      unsigned long randNumber;
      randNumber = random(minRandomNumber, maxRandomNumber);
      delay(randNumber);
      digitalWrite(ledPin, HIGH);
      timeStart = millis();
      delay(50);
      state = LIGHT_ON;
      break;

    case LIGHT_ON:
      if (buttonState == LOW) {
        timeEnd = millis();
        digitalWrite(ledPin, LOW);
        
        // set the cursor to column 0, line 1
        // (note: line 1 is the second row, since counting begins with 0):
        lcd.setCursor(0, 1);
        
        uint16_t timePassedMS = (timeEnd - timeStart);
        Serial.println(timeStart);
        Serial.println(timeEnd);
        char msg[25];
        sprintf(msg, "Pasaron %dms", timePassedMS);
        lcd.print(msg);
        Serial.println(msg);
        state = SHOW_RESULT;
      }
      break;
      
    case SHOW_RESULT:
      delay(100); 
      if (buttonState == LOW) {
        state = WAIT_BUTTON;
      }
      break;
  }
}
